import csv
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.font_manager import FontProperties

#设置宋体字体，不然中文字体无法正常显示
font_set = FontProperties(fname=r"c:\windows\fonts\simsun.ttc", size=20)

def getRefPath(path):
    log_f = open("参考路径.csv", "w+")
    for p in path:
        log_f.write("{},{}\n".format(p[0], p[1]))

# 读取csv数据
def readError(path):
    dis_error = []
    slope_error = []
    steer = []
    with open(path) as f:
        f_csv = csv.reader(f)
        # headers = next(f_csv)
        for row in f_csv:
            dis_error.append(float(row[2]))
            slope_error.append(float(row[5]))
            steer.append(float(row[6]))
            # print("{},{},{}".format(row[2], row[5], row[6]))
    control_error = [dis_error, slope_error, steer]
    return control_error

def readCsv(path):
    pose = []
    with open(path) as f:
        f_csv = csv.reader(f)
        # headers = next(f_csv)
        for row in f_csv:
            p = [float(row[0]), float(row[1])]
            pose.insert(0,p)
    return pose

def drawPic(data):
    plt.figure(figsize=(13, 8))
    color = ['black','red']
    label = ["参考路径", '实际路径']
    linestyle = ['--', '-.']
    linewidth = [2, 2]
    for j in range(len(data)):
        x = []
        y = []
        for p in data[j]:
            x.append(p[0])
            y.append(p[1])
        plt.plot(x,y,label=label[j], ls=linestyle[j], linewidth=linewidth[j], color=color[j])

    plt.legend(prop={'family':'SimSun','size':15})
    plt.xlabel('横向位置/m',fontproperties=font_set)
    plt.ylabel('纵向位置/m',fontproperties=font_set)

    plt.xlim(0, 500)
    plt.ylim(-140, 260)

    # plt.axis('off')
    plt.show()

def drawError(data):
    plt.figure(figsize=(13, 8))
    color = ['blue','red', 'black']
    label = ["distance error", 'yaw error', 'steer']
    linestyle = ['-', '-', '-.']
    linewidth = [1, 1, 1]
    for j in range(len(data)):
        x = np.arange(0, len(data[j]), 1)
        y = data[j]
        plt.title(label[j])
        plt.plot(x,y,label=label[j], ls=linestyle[j], linewidth=linewidth[j], color=color[j])
        plt.subplot(3, 1, j+1)

    # plt.legend(prop={'family':'SimSun','size':15})
    # plt.xlabel('横向位置/m',fontproperties=font_set)
    # plt.ylabel('纵向位置/m',fontproperties=font_set)


    # plt.axis('off')
    plt.show()


