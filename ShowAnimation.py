import math
from PlanningAndControl import *
import matplotlib.pyplot as plt

def ShowPlanning (obs, ego, obj_path, plan_path, speed, target_px, target_py, target_px_car, target_py_car,
                  dis_error, yaw_error, k_r, steer, throttle, brake, lateral_error, max_lateral_error, average_lateral_error, theta_ref,time):
    obs_x, obs_y, obj_x, obj_y, plan_x, plan_y, car_plan_x, car_plan_y = [], [], [], [], [], [], [], []
    ego_x, ego_y = ego[0], ego[1]
    tra_car = TrajectoryToCarCoordinates(plan_path, ego)
    for p in obs:
        obs_x.append(p[0])
        obs_y.append(p[1])
    for p in obj_path:
        obj_x.append(p[0])
        obj_y.append(p[1])
    for p in plan_path:
        plan_x.append(p[0])
        plan_y.append(p[1])
    for p in tra_car:
        car_plan_x.append(p[0])
        car_plan_y.append(p[1])

    area = 100
    plt.cla()
    # for stopping simulation with the esc key.
    plt.gcf().canvas.mpl_connect(
        'key_release_event',
        lambda event: [exit(0) if event.key == 'escape' else None])
    plt.plot(obj_x, obj_y)  # 参考路径用蓝色线画出来
    plt.plot(obs_x, obs_y, "xk")  # 障碍物画X
    plt.plot(plan_x, plan_y, "-or")  # 红色的点画出局部路径
    plt.plot(target_px, target_py, "-ob")  # 跟踪点
    plt.plot(ego_x, ego_y, "vc")  # 车体所在位置画蓝色三角
    plt.xlim(ego_x - area, ego_x + area)
    plt.ylim(ego_y - area, ego_y + area)
    # plt.title("v[km/h]:" + str(speed * 3.6)[0:4] + " 距离误差:" + str(dis_error) + "m")
    plt.title("delay:{} steer:{}° throttle:{} brake:{}° \n " .format(round(time, 6), round(steer, 3), round(throttle, 3),
                                                                      round(brake, 3)))
    plt.grid(True)
    plt.subplot(121)

    plt.cla()
    # plt.plot(obj_x, obj_y)  # 参考路径用蓝色线画出来
    # plt.plot(obs_x, obs_y, "xk")  # 障碍物画X
    plt.plot(car_plan_x, car_plan_y, "-or")  # 红色的点画出局部路径
    plt.plot(target_px_car, target_py_car, "-ob")  # 跟踪点
    plt.plot(0, 0, "vc")  # 车体所在位置画蓝色三角
    plt.xlim( - 10,  10)
    plt.ylim( - 10,  10)
    # plt.title("v[km/h]:" + str(speed * 3.6)[0:4]theta_ref + " 距离误差:" + str(dis_error) + "m")
    plt.title("LateralError:{}m  MaxError:{}m  MeanError:{}m \n theta_ref:{}° yaw_error:{}° radius:{}".format(round(lateral_error, 3),
                                    round(max_lateral_error, 3), round(average_lateral_error, 3),round(theta_ref, 3),
                                                                round(yaw_error / math.pi * 180, 3), round(k_r, 3)))
    plt.grid(True)
    plt.subplot(122)
    # plt.subplot(1, 2, 1, plt.figure(figsize=(16, 9)))
    # fig, axes = plt.subplots(1, 2, figsize=(18, 10))
    plt.pause(0.00001)

