import math
import scipy.linalg as la
import numpy as np

class LQR:

    def __init__(self, Q, R, dt, L, Cf, Cr, Lf, Lr, m, Kff, Kfb, max_steer):
        '''
        Initialization.
        '''
        self.Q = Q
        self.R = R
        # parameters
        self.dt = dt # time tick[s]
        self.L = L  # Wheel base of the vehicle [m] 2.94
        self.Cf = Cf  # 前轮侧偏刚度
        self.Cr = Cr  # 后轮侧偏刚度
        self.Lf = Lf  # 前轮轴距
        self.Lr = Lr  # 后轮轴距
        self.m = m  # 车身质量
        self.Kff = Kff # 前馈控制量比例系数
        self.Kfb = Kfb # 反馈控制量比例系数
        self.max_steer = max_steer  # maximum steering angle[rad]

    def pi_2_pi(self, angle):
        return (angle + math.pi) % (2 * math.pi) - math.pi

    def dlqr(self, A, B, Q, R):
        """Solve the discrete time lqr controller.
        x[k+1] = A x[k] + B u[k]
        cost = sum x[k].T*Q*x[k] + u[k].T*R*u[k]
        # ref Bertsekas, p.151
        """
        # first, try to solve the ricatti equation
        X, OK = self.solve_DARE(A, B, Q, R)
        if not OK:
            print("黎卡提方程不收敛！！！")

        # compute the LQR gain
        K = la.inv(B.T @ X @ B + R) @ (B.T @ X @ A)

        eigVals, eigVecs = la.eig(A - B @ K)

        return K, X, eigVals

    def solve_DARE(self, A, B, Q, R):
        """
        solve a discrete time_Algebraic Riccati equation (DARE)
        """
        X = Q
        maxiter = 500
        eps = 0.001
        OK = False
        for i in range(maxiter):
            Xn = A.T @ X @ A - A.T @ X @ B @ \
                la.inv(R + B.T @ X @ B) @ B.T @ X @ A + Q
            if (abs(Xn - X)).max() < eps:
                OK = True
                break
            X = Xn

        return Xn, OK

    def ComputeFeedForward(self, kappa,v,k):
        kv = self.Lr * self.m / 2 / self.Cf / self.Lr - self.Lf * self.m / 2 / self.Cf / self.L
        steer_angle_feedforward =  (self.L * kappa + kv * v * v * kappa -
             k * (self.Lr * kappa - self.Lf * self.m * v * v * kappa / 2 / self.Cr / self.L))
        return steer_angle_feedforward

    def lqr_steering_control(self, k, v_ref, theta_ref, x_error, y_error, yaw_error):
        delta_ref = math.atan(self.L * k)

        A = np.eye(3)
        A[0, 2] = -self.dt * v_ref * math.sin(theta_ref)
        A[1, 2] = self.dt * v_ref * math.cos(theta_ref)
        # print(A)
        #
        # print(A)

        B = np.zeros((3, 2))
        B[0, 0] = self.dt * math.cos(theta_ref)
        B[1, 0] = self.dt * math.sin(theta_ref)
        B[2, 0] = self.dt * math.tan(delta_ref) / self.L
        B[2, 1] = self.dt * v_ref / (self.L * math.cos(delta_ref)**2)

        K, _, _ = self.dlqr(A, B, self.Q, self.R)

        x = np.zeros((3, 1))
        x[0, 0] = x_error
        x[1, 0] = y_error
        x[2, 0] = yaw_error

        ff = self.ComputeFeedForward(k, v_ref, -K[1,0])
        fb = self.pi_2_pi((-K @ x)[1, 0])
        delta = self.Kff * ff + self.Kfb * fb
        print("ff:{}  fb:{}  delta{}".format(fb, ff, delta))

        return  delta

    # steer = lqr_steering_control(1/-12.9277469012064, 10.52 ,9.4730841454551, 19.4730841454551, 0.520040732900878)


    # steer = lqr_steering_control(1/-7.2819291207854, 5, 1.66117948357403, 0.649310417664053, 0.0256568336101539, 0.267076719944368)
    # steer = lqr_steering_control(1/-7.2819291207854, 27.3/3.6, 1.66117948357403, 0.649310417664053, 0.0256568336101539, 0.267076719944368)
    # print(steer*425.0)
    # steer = lqr_steering_control(1/8, 5, math.pi/2, -1, -1, 0)
    # # steer = lqr_steering_control(-1/8.9444, 36.5/3.6, math.radians(11.5), -0.909, 3.743, math.radians(43.225))
    # print(steer)