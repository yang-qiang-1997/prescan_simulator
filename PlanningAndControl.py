import matplotlib.pyplot as plt
import numpy as np
import math
import csv
import os
from scipy import optimize
import globalManger  as gm

"""角度转化为区间[0, 2pi]

Args:
    angle: 待转换角度

Returns:
    angle: 转换后的角度

Raises:
"""
def ZeroTo2Pi(angle):
    while angle < 0:
        angle += 2 * math.pi
    while angle > 2 * math.pi:
        angle -= 2 * math.pi
    return angle

"""Prescan车辆航向角转化为[0, 2pi]

Args:
    angle: 待转换角度

Returns:
    angle: 转换后的角度

Raises:
"""
def NormalizeCarAngle(angle):
    angle = -angle + math.pi / 2
    angle = ZeroTo2Pi(angle)
    return angle


"""世界坐标系下的点转化为车体坐标系下的点
目标点先绕自车坐标旋转theta，然后平移

Args:
    car_x,car_y,car_theta: 自车位姿
    px,py: 目标点坐标

Returns:
    x, y: 车体坐标下的坐标

Raises:
"""
def CarCoordinates(car_x, car_y, car_theta, px, py):
    car_theta = math.radians(car_theta)
    x = (px - car_x) * math.cos(car_theta) - (py - car_y) * math.sin(car_theta) + car_x
    y = (px - car_x) * math.sin(car_theta) + (py - car_y) * math.cos(car_theta) + car_y
    x = x - car_x
    y = y - car_y
    return x, y

"""计算点到直线距离
Args:
    point: [x0, y0]
    line: [x1, y1, x2, y2]
"""
def get_distance_point2line(point, line):
    line_point1, line_point2 = np.array(line[0:2]), np.array(line[2:])
    vec1 = line_point1 - point
    vec2 = line_point2 - point
    distance = np.abs(np.cross(vec1, vec2)) / np.linalg.norm(line_point1 - line_point2)
    return distance


"""世界坐标系下的轨迹转化为车体坐标轨迹

Args:
    trajectory: list 轨迹点集合 as[[x1,y1],[x2,y2]...]
    ego_car: 自车状态[x,y,v,theta]

Returns:
    new_trajectory：车体坐标系的轨迹

Raises:
"""
def TrajectoryToCarCoordinates(trajectory, ego_car):
    new_trajectory = []
    for p in trajectory:
        x, y = CarCoordinates(ego_car[0], ego_car[1], ego_car[3], p[0], p[1])
        new_trajectory.append([x, y])
    # print(new_trajectory)
    return new_trajectory

"""三次方程弧度区间[-pi, pi]
"""
def AngleToPi(heading):
    while heading < -math.pi:
        heading = heading + 2 * math.pi
    while heading > math.pi:
        heading = heading - 2* math.pi
    return heading

"""三次方程
Args:
    x: x坐标
    k0, k1, k2, k3: 三次方程系数

Returns:
    y：y坐标

Raises:
"""
def CubicFunction(x, k0, k1, k2, k3):
    y = k0 + k1 * x + k2 * pow(x, 2) + k3 * pow(x, 3)
    return y

def CalcMinDist(target_trajectory, ego_State):
    min_dis = 1e9
    min_ind = 0
    # 选择最近点
    for i in range(len(target_trajectory)):
        point = target_trajectory[i]
        cur_dis = pow(point[0] - ego_State[0], 2) + \
                  pow(point[1] - ego_State[1], 2)
        if cur_dis < min_dis and CheckDir(ego_State[3], target_trajectory[i][2]):
            min_dis = cur_dis
            min_ind = i
    gm.set_value("min_dist", min_dis)
    gm.set_value("min_dist_ind", min_ind)
    return min_dis, min_ind

def CalcLateralError (target_trajectory, ego_State):
    min_dis = 1e9
    min_ind = 0
    # 选择最近点
    for i in range(len(target_trajectory)):
        point = target_trajectory[i]
        cur_dis = pow(point[0] - ego_State[0], 2) + \
                  pow(point[1] - ego_State[1], 2)
        if cur_dis < min_dis and CheckDir(ego_State[3], target_trajectory[i][2]):
            min_dis = cur_dis
            min_ind = i
    return min_dis, min_ind

def CalcPreviewNum(target_trajectory, target_point_index, pre_dis):
    start_index = (target_point_index-5) % len(target_trajectory)
    sum_dis = 0
    for i in range(start_index, start_index+10):
        i = i % len(target_trajectory)
        j = (i+1) % len(target_trajectory)
        x1 = target_trajectory[i][0]
        y1 = target_trajectory[i][1]
        x2 = target_trajectory[j][0]
        y2 = target_trajectory[j][1]
        sum_dis += CalcDistance(x1,y1,x2,y2)
    ave_dis = sum_dis / 10
    preview_num = int(pre_dis / ave_dis)
    return preview_num

"""计算点的斜率和曲率
将点集拟合成三次曲线,并计算点的斜率和曲率

Args:
    road_x: list 点集的x坐标
    road_y: list 点集的y坐标
    x: 目标点的x坐标

Returns:
    y_fir:一阶导数
    curvature:曲率

Raises:
"""
# def CaclSlope(road_x, road_y, x):
#     k0, k1, k2, k3= optimize.curve_fit(CubicFunction, road_x, road_y, method='lm')[0]
#     y_fir = k1 + 2 * k2 * x + 3 * k3 * x * x
#     # road_y = CubicFunction(road_x, k0, k1, k2, k3)
#     return y_fir

def CaclYawRef(x, y):
    x_fir = x[1] - x[0]
    y_fir = y[1] - y[0]
    yaw_ref = math.atan2(y_fir, x_fir)
    return yaw_ref

"""两点欧式距离
Args:
    x1, y1: p1坐标
    x2, y2: p2坐标

Returns:
   p1、p2距离

Raises:
"""
def CalcDistance(x1, y1, x2, y2):
    return math.sqrt(pow(x1 - x2, 2) + \
                     pow(y1 - y2, 2))

"""计算曲率
"""
def CalcCurv(x, y):
    x_fir = x[2] - x[1]
    x_sec = (x[2] - x[1]) - (x[1] - x[0])
    y_fir = y[2] - y[1]
    y_sec = (y[2] - y[1]) - (y[1] - y[0])
    return (x_sec*y_fir-x_fir*y_sec) / (x_fir**2+y_fir**2)**1.5 + 1e-19

# x = [-0.03313072491181046, 0.02002006485159491, 0.020020064851591357]
# y = [7.8955336916649985, 6.396841030568079, 6.396841030568079]
# print(CalcCurv(x,y))

"""检查跟踪点朝向是否符合车头朝向
Args:
    degree1: 车头角度
    degree2: 跟踪点朝向

Returns:
   True/False:符合/不符合

Raises:
"""
def CheckDir(degree1, degree2):
    diff = abs(degree1 - degree2)
    if 0 < diff < 60 or 300 < diff < 360:
        return True
    else:
        return False

"""在目标点前后选n个点
用于拟合曲线，计算斜率和曲率
Args:
    target_trajectory: 参考轨迹点集合 as[[x1,y1],[x2,y2]...]
    num: 选点个数
    index: 目标点下标

Returns:
   points_x : [x1,x2,x3...]
   points_y : [y1,y2,y3...]

Raises:
"""
def SelectPoints(target_trajectory, num, index):
    points_x, points_y = [], []
    start_index = (index - num / 2) % len(target_trajectory)
    for i in range(int(start_index), int(start_index + num)):
        i = i % len(target_trajectory)
        x = target_trajectory[i][0]
        y = target_trajectory[i][1]
        points_x.append(x)
        points_y.append(y)
    return points_x, points_y

"""选择跟踪点
从参考轨迹中选择最近点,前方的第3个点作为跟踪点,并计算该点的斜率、曲率

Args:
    test_points: 参考轨迹点集合 as[[x1,y1],[x2,y2]...]
    ego_State: 自车状态[x,y,v,theta]

Returns:
   traget_point_world: 世界坐标系下目标点的位置 [x,y]
   traget_point_car: 车体坐标系下的目标点位置、距离误差[x,y,dis_error,yaw_error,k]

Raises:
"""
def GetTargetPoint(target_trajectory, ego_State, preview_dis):
    n = len(target_trajectory)
    if n  < 1:
        print("错误！读取不到路径点！")
    target_point_world = [] # 世界坐标系下跟踪点位姿[X, Y, yaw]
    target_point_car = []

    # 最近点距离和最近点下标
    min_dis, target_point_index = CalcMinDist(target_trajectory, ego_State)
    # 预瞄多少个点
    preview_num = CalcPreviewNum(target_trajectory, target_point_index, preview_dis)
    # a确定预瞄方向，并计算预瞄点下标
    a =  IsFrontPoint(target_trajectory[(target_point_index + preview_num) % len(target_trajectory)][0],
                      target_trajectory[(target_point_index + preview_num) % len(target_trajectory)][1],
                      ego_State)
    target_point_index = (target_point_index + a * preview_num) % len(target_trajectory)
    # 预瞄点世界坐标系
    theta_ref = NormalizeCarAngle(math.radians(target_trajectory[target_point_index][2])) # 参考点航向角
    target_point_world.append(target_trajectory[target_point_index][0]) # x
    target_point_world.append(target_trajectory[target_point_index][1]) # y
    target_point_world.append(target_trajectory[target_point_index][2]) # y
    gm.set_value("target_point_world",target_point_world)
    gm.set_value("target_point_index", target_point_index)
    gm.set_value("theta_ref", theta_ref)

    # 预瞄点转化为车体坐标系
    x, y = CarCoordinates(ego_State[0], ego_State[1], ego_State[3], target_point_world[0], target_point_world[1])
    target_point_car.append(x)
    target_point_car.append(y)
    gm.set_value("target_point_car", target_point_car)


"""判断点是否在车头前方
车体坐标系内y>0的点作为预瞄点

Args:
    x, y: 点坐标
    ego_State: 自车状态[x,y,v,theta]

Returns:
   True:在前方
   False:不在前方

Raises:
"""
def IsFrontPoint(x, y, ego_State):
    x, y = CarCoordinates(ego_State[0], ego_State[1], ego_State[3], x, y)
    if y > 0:
        return 1
    else:
        return -1

def UpdateErrorInfo(target_trajectory, ego_State):
    # 计算横向误差
    min_ind = gm.get_value("min_dist_ind")
    line = [target_trajectory[min_ind][0],
            target_trajectory[min_ind][1],
            target_trajectory[(min_ind+1)%len(target_trajectory)][0],
            target_trajectory[(min_ind+1)%len(target_trajectory)][1]]
    ego_car_point = [ego_State[0], ego_State[1]]
    lateral_error = get_distance_point2line(ego_car_point, line)
    gm.set_value("lateral_error", lateral_error)

    # 计算状态误差 X,Y,YAW
    target_point_world = gm.get_value("target_point_world")
    x_error = ego_State[0] - target_point_world[0]
    y_error = ego_State[1] - target_point_world[1]
    yaw_error = math.radians(ego_State[3] - target_point_world[2])
    if yaw_error < -math.pi:
        yaw_error =  yaw_error + 2 * math.pi
    elif yaw_error > math.pi:
        yaw_error = yaw_error - 2 * math.pi
    gm.set_value("x_error", x_error)
    gm.set_value("y_error", y_error)
    gm.set_value("yaw_error", yaw_error)

def UpdateRoadInfo(target_trajectory, ego_State):
    n = len(target_trajectory)
    target_point_index = gm.get_value("target_point_index")
    # 跟踪点前后10个点作为局部路径，用于显示
    start_index = (target_point_index - 10) % n
    tmp_points_x = []
    tmp_points_y = []
    for i in range(start_index, start_index + 20):
        i = i % n
        x = target_trajectory[i][0]
        y = target_trajectory[i][1]
        x, y = CarCoordinates(ego_State[0], ego_State[1], ego_State[3], x, y)
        tmp_points_x.append(x)
        tmp_points_y.append(y)
    gm.set_value("local_path_x", tmp_points_x)
    gm.set_value("local_path_y", tmp_points_y)
    ind = 10 # 预瞄点下标
    tar_kappa = CalcCurv(tmp_points_x[ind-1:ind+2], tmp_points_y[ind-1:ind+2])
    print(tmp_points_x[ind-1:ind+2])
    print(tmp_points_y[ind-1:ind+2])
    gm.set_value("tar_kappa", tar_kappa)

"""PID横向控制器
Args:
    target_point: 跟踪点[x,y]
    ego_State: 自车状态[x,y,v,theta]
    dis_pid: 横向距离PID
    yaw_pid: 航向角PID

Returns:
   steer: 方向盘转角
   dis_error: 距离误差
   yaw_error: 航向角误差
   target_point[3]: 自车theta

Raises:
"""
def LateralControlByPID(target_point, ego_State, dis_pid, yaw_pid):
    dis_error = target_point[0]
    yaw_error = math.atan(target_point[2])
    if yaw_error > 0 :
        yaw_error = math.pi / 2 - abs(math.atan(target_point[2]))
    elif yaw_error < 0 :
        yaw_error = abs(math.atan(target_point[2])) - math.pi / 2
    dis_pid.update(dis_error)
    steer = dis_pid.output
    yaw_pid.update(yaw_error  + steer)
    steer = yaw_pid.output
    yaw_pid = steer
    ControlErrorLog(ego_State[1], target_point[1], dis_error, math.radians(ego_State[3]),
                    0, yaw_error, steer, dis_pid, yaw_pid)
    return steer, dis_error, yaw_error, target_point[3]

"""生成直线参考轨迹
Args:

Returns:
   test_points: 轨迹点集合 as[[x1,y1],[x2,y2]...]

Raises:
"""
def GetTestTrajectory(): # Hz=10, dt=0.1s, 10 m/s -> 1 m/dt
    test_points = []
    start_y = 52.5222 # 52.5222
    start_x = 8
    for x in np.arange(8, 1500, 0.5):
        point = [x, start_y, 90]
        test_points.append(point)
    print(test_points)
    return test_points

"""生成圆参考轨迹 r=52.52
"""
def GetTestTrajectory2(): # 圆
    test_points = []
    for theta in np.arange(0, 2*math.pi, 2 * math.pi / 360):
        x = 52.52 * math.cos(theta) + 8
        y = 52.52 * math.sin(theta)
        point = [x, y]
        test_points.append(point)
    print(test_points)
    return test_points

"""生成测试场地参考轨迹 轨迹点集在csv文件中
"""
def GetTestTrajectory3(): # 院子路线
    path = "reference.csv"
    test_points = []
    with open(path) as f:
        f_csv = csv.reader(f)
        x, y, pre_x, pre_y, theta_p, degree_p, i = 0, 0, 0, 0, 0, 0, 0
        for row in f_csv:
            x, y = float(row[0]), float(row[1])
            if i != 0:
                dx =  x - pre_x
                dy =  y - pre_y
                theta_p = math.atan2(dy, -dx) - math.pi / 2
                degree_p = math.degrees(ZeroTo2Pi(theta_p))
            pre_x = x
            pre_y = y
            i += 1
            p = [x, y, degree_p] # 坐标和朝向
    test_points[-1][2] = test_points[-2][2]
    return test_points

def GetTestTrajectory4(): # 顺时针圆轨迹
    test_points = []
    i = 0
    pre_x = 0
    pre_y = 0
    degree_p = 0
    for theta in np.arange(0, 2*math.pi, 2 * math.pi / 360):
        x = 52.52 * math.cos(theta) + 8
        y = 52.52 * math.sin(theta)
        if i!=0:
            dx = pre_x - x
            dy = pre_y - y
            theta_p = math.atan2(dy, -dx) - math.pi / 2
            degree_p = math.degrees(ZeroTo2Pi(theta_p))
        pre_x = x
        pre_y = y
        i += 1
        point = [x, y, degree_p]
    test_points[-1][2] = test_points[-2][2]
    return test_points

def GetTestTrajectory5(): # 逆时针圆轨迹
    test_points = []
    i = 0
    pre_x = 0
    pre_y = 0
    degree_p = 0
    for theta in np.arange(2*math.pi, 0, -2 * math.pi / 360):
        x = 53 * math.cos(theta) + 8
        y = 53 * math.sin(theta)
        if i!=0:
            dx = pre_x - x
            dy = pre_y - y
            theta_p = math.atan2(dy, -dx) - math.pi / 2
            degree_p = math.degrees(ZeroTo2Pi(theta_p))
        pre_x = x
        pre_y = y
        i += 1
        point = [x, y, degree_p]
        test_points.append(point)
    test_points[-1][2] = test_points[-2][2]
    return test_points

def GetTestTrajectory8(): # 8字
    path = "road_loop8.csv"
    test_points = []
    with open(path) as f:
        f_csv = csv.reader(f)
        x, y, pre_x, pre_y, theta_p, degree_p, i = 0, 0, 0, 0, 0, 0, 0
        for row in f_csv:
            x, y = float(row[0]), float(row[1])
            if i != 0:
                dx = x - pre_x
                dy = y - pre_y
                theta_p = math.atan2(dy, -dx) - math.pi / 2
                degree_p = math.degrees(ZeroTo2Pi(theta_p))
            pre_x = x
            pre_y = y
            i += 1
            p = [x, y, degree_p]  # 坐标和朝向
            test_points.insert(0, p)
    test_points[-1][2] = test_points[-2][2]
    return test_points

"""生成误差日志
"""
def ControlErrorLog(ego_y, ref_y, dis_error, ego_yaw, ref_yaw, yaw_error, steer, dis_pid, yaw_pid):
    log = open("误差日志.csv", "a+")
    file = "误差日志.csv"
    if not os.path.getsize(file):
        log.write("ego_y, ref_y, dis_erro, ego_yaw, ref_yaw, yaw_error, steer, dis_pid, yaw_pid\n")
    log.write("{},{},{},{},{},{},{},{},{}\n".format(ego_y, ref_y, dis_error,ego_yaw,
                                    ref_yaw, yaw_error, steer, dis_pid, yaw_pid))

def ControlErrorLog2(lateral_error,x_error,y_error,yaw_error,k,steer):
    log = open("误差日志.csv", "a+")
    file = "误差日志.csv"
    if not os.path.getsize(file):
        log.write("lateral_error,x_error,y_error,yaw_error,k,steer\n")
    log.write("{},{},{},{},{},{}\n".format(lateral_error,x_error,y_error,yaw_error,k,steer))