import DrawPic
def test():
    path_name1 = '参考路径.csv'
    path_name2 = '实际路径.csv'

    pose1 = DrawPic.readCsv(path_name1)
    pose2 = DrawPic.readCsv(path_name2)

    # 多条轨迹组成数据集 [pose1, pose2, pose3...]
    data = [pose1, pose2]
    DrawPic.drawPic(data)


    # path_name3 = '误差日志.csv'
    # data = DrawPic.readError(path_name3)
    # DrawPic.drawError(data)

# test()