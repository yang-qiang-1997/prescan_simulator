# Prescan_simulator
![img_3.png](img_3.png)
#### 介绍
Prescan自动驾驶仿真，用于验证规划控制算法。<br>
横向控制:LQR、模糊PID(弃用)<br>
纵向控制:PID<br>
全局规划:静态全局轨迹点，文件reference.csv<br>
局部规划:EM Planner(待更新)<br>
<br>
#### 软件架构
python、simulink、matlab2018b、prescan2021.1.0 64 bits<br>
规划控制模块使用python搭建<br>
环境搭建来自于prescan<br>
动力学仿真来自于simulink<br>
程序和prescan使用UDP通信<br>

#### 使用教程

1. prescan打开road文件夹中road.pex文件<br>
2. 点击Build,点击Invoke Simulink run mode<br>
![img.png](img.png)
3. matlab在Road目录下，启动Road_cs.slx文件<br>
![img_1.png](img_1.png)
4. 点击Regenerate，然后点击Run<br>
![img_2.png](img_2.png)
5. 运行BasicCarController.py文件<br>


