import math
import socket
import time
import array
import threading
import struct
import os
import signal
import timeit

import PlanningAndControl as pnc
import globalManger as gm
import DrawPic
import ShowAnimation

from PID import *
from lqr import *


def CtrlC():
    print("exit")
    os._exit(0)

# server
# listen port
EgoStateIP = "127.0.0.1"
EgoStatePORT = 8001
ObstacleListIP = "127.0.0.1"
ObstacleListPORT = 8002
ControlIP = "127.0.0.1"
ControlPORT = 8000

EgoStateSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
ObstacleListSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
EgoStateAddress = (EgoStateIP, EgoStatePORT)
EgoStateSocket.bind(EgoStateAddress)
EgoStateSocket.settimeout(10)  # set timeout for lone time no new message received
ObstacleListAddress = (ObstacleListIP, ObstacleListPORT)
ObstacleListSocket.bind(ObstacleListAddress)
ObstacleListSocket.settimeout(10)  # set timeout for lone time no new message received
# client
ControlSocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
ControlServerAddress = (ControlIP, ControlPORT)

# global var(ego state, obslist, ...)
# egoState = [49.0, 42.53, 10.0, 90.0] # [X, Y, V, Theta]
ConnSucc = False
egoState = [0.0,0.0,0.0,0.0] # [X, Y, V, Theta]
obsList = []
controlCMD = [0.0, 0.0, 0.0]
objectPath = []
planPath = []
lon_con = PID(6, 0.01, 0.1)
# code
def FloatTo8Bytes(f):
    bs = struct.pack("d",f)
    return bs

# distance
# def LateralControl(y, target_y):
#     '''
#     if y > target_y: ego car is above target line, turn right, steer > 0
#     or the opposite
#     '''
#     steer = -2 * (y - target_y)
#     return steer

# acc
def LongitudinalControl(v, target_v, obs_dis):
    '''
    if target_v > v: do acc, or the opposite
    besides, if there is anyone close to ego car, do dec, more closer more dec
    '''
    lon_con.update(v - target_v)
    acc = lon_con.output
    print("acc:{}".format(acc))
    # if obs_dis < 15 and obs_dis >= 0:
    #     acc = min(acc, (15-obs_dis)*(-3))
    #     print('acc data',acc)
    return acc

# throttle and brake acconut
def ExecutiveControl(acc):
    '''
    transfer acc to throttle & brake(if acc do throttle, else do brake)
    '''
    throttle = 0.0
    brake = 0.0
    if acc > 0:
        throttle = min(30, 10*acc)
    else:
        brake = min(max(1, -acc*10), 10)
    return throttle, brake

def ControlThreadFunc():
    '''
    Main Control Logic
    steer and acc(dec)
    '''
    logPrefix = "[Control]"
    targetVelocity = 11.11# target speed 6m/s
    objectPath = pnc.GetTestTrajectory8()
    # objectPath += pnc.GetTestTrajectory7() 测试朝向选点，双环轨迹
    planPath = objectPath
    # 20m/s
    # dis_con = Fuzzy_PID.Fuzzy_PID(-0.29066 / 8.4, -0.07266666 / 10, 0, 0, -0.29066 / 3, -0.07266666 / 10)
    # slope_con = Fuzzy_PID.Fuzzy_PID(825.68807 / 0.62, 206.42201 / 10, 825.68807 / 3.5, 206.42201 / 100, 825.68807 / 1,
    #                                206.42201 / 10)
    DrawPic.getRefPath(objectPath)
    actual_tra = open("实际路径.csv", "w+")
    lateral_errors = []
    max_lateral_error = 0
    average_lateral_error = 0

    # parameters
    preview_dis = 7 # 预瞄距离 m
    steer_ratio = 5.65 # 方向盘传动比
    Q = np.diag([1, 1, 40])
    R = np.diag([1, 1])
    Kff = 1
    Kfb = 1
    dt = 0.1  # time tick[s]
    L = 2.94  # Wheel base of the vehicle [m] 2.94
    Cf = 155494.663  # 前轮侧偏刚度
    Cr = 155494.663  # 后轮侧偏刚度
    Lf = 1.17  # 前轮轴距
    Lr = 1.77  # 后轮轴距
    m = 1930  # 车身质量
    max_steer = np.deg2rad(45.0)  # maximum steering angle[rad]
    lqr_con = LQR(Q, R, dt, L, Cf, Cr, Lf, Lr, m, Kff, Kfb, max_steer)
    while True:
        if True and ConnSucc:
            start = timeit.default_timer()
            global egoState, obsList
            print("egodata", egoState)
            print("obsdata:", obsList)
            obsListCache = obsList.copy() # for data with potential large size, do copy before use(if no lock for multi-thread)
            pnc.GetTargetPoint(objectPath, egoState, preview_dis)
            pnc.UpdateErrorInfo(objectPath, egoState)
            pnc.UpdateRoadInfo(objectPath, egoState)
            # steer, dis_error, yaw_error, k = pnc.LateralControlByPID(target_point_car, egoState, dis_con, slope_con)

            lateral_error = gm.get_value("lateral_error")
            x_error = gm.get_value("x_error")
            y_error = gm.get_value("y_error")
            yaw_error = gm.get_value("yaw_error")
            tar_kappa = gm.get_value("tar_kappa")
            theta_ref = gm.get_value("theta_ref")
            current_v = egoState[2]
            target_point_world = gm.get_value("target_point_world")
            target_point_car = gm.get_value("target_point_car")

            lateral_errors.append(lateral_error)
            max_lateral_error = lateral_error if lateral_error > max_lateral_error else max_lateral_error
            average_lateral_error = np.mean(lateral_errors)

            # steer = (lqr_steering_control(k, egoState[2], dis_error_list[-1], yaw_error_list[-1], dis_error_list[-2], yaw_error_list[-2]))*450
            steer = lqr_con.lqr_steering_control(tar_kappa, current_v, theta_ref, x_error, y_error, -yaw_error)
            print("lqr input:{}, {}, {}, {}, {}, {}".format(tar_kappa, current_v, theta_ref, x_error, y_error, yaw_error))
            steer = math.degrees(steer) * steer_ratio
            print('ego1data',egoState[1])
            obsDis = 100 # default 100m obsdis(dont care)
            # a simple single-lane scenario
            # y^
            #  |--------------------------(upper bound, egoY+1.5)
            #  |[=ego=>]|<-- obsDis -->|o
            #  |--------------------------(lower bound, egoY-1.5)
            #  ------------------------------------------------------> x
            egoX, egoY = egoState[0], egoState[1]
            actual_tra.write("{},{}\n".format(egoX, egoY))
            for obs in obsListCache:
                if abs(obs[1] - egoY) < 1.5:
                    if obs[0] > egoX:
                        obsDis = min(obsDis, obs[0]-egoX)
            acc = LongitudinalControl(egoState[2], targetVelocity, obsDis)
            throttle, brake = ExecutiveControl(acc)
            msg =  FloatTo8Bytes(steer)
            msg += FloatTo8Bytes(throttle)
            msg += FloatTo8Bytes(brake)
            controlCMD = [steer, throttle, brake]
            ControlSocket.sendto(msg, ControlServerAddress)
            now = time.time()
            print(logPrefix, time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(now)))
            print(logPrefix, "state:", egoState)
            print(logPrefix, "ctrl:", controlCMD)
            end = timeit.default_timer()  # end： 8.5791582
            print('spend： %s second' % (end - start))
            ShowAnimation.ShowPlanning(obsList, egoState, objectPath, planPath, egoState[2], target_point_world[0], target_point_world[1],
                                       target_point_car[0], target_point_car[1], 0, yaw_error,1/tar_kappa,steer,throttle,brake,
                                       lateral_error,  max_lateral_error, average_lateral_error, math.degrees(theta_ref), end - start)
            pnc.ControlErrorLog2(lateral_error,x_error,y_error,yaw_error,1/tar_kappa,steer)
            time.sleep(0.01)
        else:
            print(logPrefix, "unexpected exit")
            # break

# thread function define
def UpdateEgoStateThreadFunc():
    '''
    thread function: receive ego state from simulator
    '''
    logPrefix = "[EgoStateThread]"
    msgFreq = 0.0
    msgCounter = 0.0
    lastCounterTick = time.time()

    while True:
        try:
            now = time.time()
            receiveData, client = EgoStateSocket.recvfrom(1024)
            global ConnSucc
            ConnSucc = True
            doublesSequence = array.array('d', receiveData)
            tmpX = 0
            tmpY = 0
            tmpTheta = 0.0
            tmpV = 0.0
            if len(doublesSequence) < 4: # invalid msg, early break
                continue
            tmpX, tmpY, tmpV, tmpTheta = doublesSequence[0], doublesSequence[1], doublesSequence[2], doublesSequence[3]
            # update state
            global egoState
            egoState = [tmpX, tmpY, tmpV, tmpTheta]
            # update msg status
            msgCounter += 1
            if (now-lastCounterTick) > 1: # calc frequency per second
                msgFreq = msgCounter / (now-lastCounterTick)
                msgCounter = 0.0
                lastCounterTick = now
                print(logPrefix, "Freq:", msgFreq, "Hz")
                if msgFreq < 5:
                    print(logPrefix, "unexpected low freq")
            time.sleep(0.01)
        except socket.timeout:
            print(logPrefix, "time out")

def UpdateObstacleStateThreadFunc():
    '''
    thread function: receive obstacles state from simulator
    '''
    logPrefix = "[ObstacleStateThread]"
    msgFreq = 0.0
    msgCounter = 0.0
    lastCounterTick = time.time()
    while True:
        try:
            now = time.time()
            receiveData, client = ObstacleListSocket.recvfrom(1024)
            doublesSequence = array.array('d', receiveData)
            tmpObsList = []
            if len(doublesSequence)%5 != 0: # invalid msg, early break
                continue
            for i in range(0, len(doublesSequence), 5):
                tmpX, tmpY, tmpTheta, tmpW, tmpL  = doublesSequence[i+0], doublesSequence[i+1], doublesSequence[i+2], doublesSequence[i+3], doublesSequence[i+4]
                tmpObsList.append((tmpX, tmpY, tmpTheta, tmpW, tmpL))
            # update state
            global obsList
            obsList = tmpObsList
            # update msg status
            msgCounter += 1
            if (now-lastCounterTick) > 1: # calc frequency per second
                msgFreq = msgCounter / (now-lastCounterTick)
                msgCounter = 0.0
                lastCounterTick = now
                print(logPrefix, "Freq:", msgFreq, "Hz")
                if msgFreq < 5:
                    print(logPrefix, "unexpected low freq")
            time.sleep(0.01)
        except socket.timeout:
            print(logPrefix, "time out")

def TestLateralControl():
    # case 1. car is above ref line
    steer = LateralControl(2.0, 1.75)
    if steer <= 0:
        print('want turn right, but turn left')
        return False
    # case 2. car is below ref line
    steer = LateralControl(1.5, 1.75)
    if steer >= 0:
        print('want turn left, but turn right')
        return False
    return True

def TestLongitudinalControl():
    # scenario 1. no obstacle(obsDis is +inf => 100m)
    # case 1. faster than target speed
    acc = LongitudinalControl(10.0, 6.0, 100)
    if acc >= 0:
        print('want dec, but acc')
        return False
    # case 2. slower than target speed
    acc = LongitudinalControl(0.0, 6.0, 100)
    if acc <= 0:
        print('want acc, but dec')
        return False
    # scenario 2. obstacle near
    # case 1. slower than target speed
    acc = LongitudinalControl(1.0, 6.0, 5)
    if acc >= 0:
        print('want dec, but acc')
        return False
    return True

def ParameterInitialization():
    return

if __name__ == "__main__":
    signal.signal(signal.SIGINT, CtrlC)
    signal.signal(signal.SIGTERM, CtrlC)

    t1 = threading.Thread(target=UpdateEgoStateThreadFunc)
    t2 = threading.Thread(target=UpdateObstacleStateThreadFunc)
    t3 = threading.Thread(target=ControlThreadFunc) 
    t1.setDaemon(True)
    t2.setDaemon(True)
    t3.setDaemon(True)
    
    t1.start()
    t2.start()
    t3.start()

    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break
    
    t1.join()
    t2.join()
    t3.join()
