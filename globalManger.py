#初始化一个空的键值对字典
# def _init():
global _global_dict
_global_dict = {}

#设置字典内容
def set_value(name, value):
    _global_dict[name] = value

#读取字典内容
def get_value(name, defValue=None):
    try:
        return _global_dict[name]
    except KeyError:
        return defValue
